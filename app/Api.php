<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    protected $table = 'rest';
    protected $fillable = ['pesan','user_id'];
}
