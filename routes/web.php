<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',"ApiController@index");
Route::post('/rest/api',"ApiController@action");
Route::post('/registrasi',"ApiController@action");
Route::get('Konfirmasiemail/{email}/{register_token}',"Auth\RegisterController@Konfirmasiemail")->name('Konfirmasiemail');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
